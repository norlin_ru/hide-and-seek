// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Templates/SubclassOf.h"
#include "HSSpawner.generated.h"

class APawn;
class UBoxComponent;

class AHSCharacter;
class AHSItemBase;

UCLASS()
class HS_API AHSSpawner : public AActor
{
	GENERATED_BODY()

public: // AActor

	AHSSpawner();

protected: // AActor

	virtual void BeginPlay() override;

protected: // Internal methods

	// Return randomized location for spawning a new object with specified Extent
	// If OutLocation is set to non-zero vector - will try to use that location as close as possible
	// if bRespectSafeZone == true, will not use the safe-zone tiles for spawning
	bool GetRandomLocation(const FVector& Extent, FVector& OutLocation, bool bRespectSafeZone = false);

	// Return the tile index where the Origin is located
	int32 FindTileIndex(const FVector& Origin) const;

	// Return true if the TileIndex is for the player tile or adjacent tiles
	bool IsSafeZone(int32 TileIndex) const;

	// Prepare the spawn grid
	void PrepareSpawnTiles();

	// Spawn a new object of ObjectClass
	// if bRespectSafeZone == true, will not use the safe-zone tiles for spawning
	// If DesiredLocation is set to non-zero vector - will try to use that location as close as possible
	UFUNCTION(BlueprintCallable, Category = "Hide and Seek|Spawn")
	AActor* SpawnActor(TSubclassOf<AActor> ObjectClass, bool bRespectSafeZone = false, const FVector& DesiredLocation = FVector::ZeroVector);

	void SpawnEnemies();
	void SpawnObstacles();
	void SpawnItems();

protected: // Settings

	// How many enemies to spawn
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Hide and Seek|Spawn")
	int32 EnemiesAmount;

	// Enemy class
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Hide and Seek|Spawn")
	TSubclassOf<AHSCharacter> EnemyClass;

	// A radius for the safe zone - the distance from player where should be no enemies in clear sight
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Hide and Seek|Spawn")
	float SafeZoneRadius;

	// How many Obstacles to spawn
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Hide and Seek|Spawn")
	int32 ObstaclesAmount;

	// Obstacle class
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Hide and Seek|Spawn")
	TSubclassOf<AActor> ObstacleClass;

	// How many Items to spawn
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Hide and Seek|Spawn")
	int32 ItemsAmount;

	// Item class
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Hide and Seek|Spawn")
	TSubclassOf<AHSItemBase> ItemClass;

private: // Components

	// The box area used to detect the spawning zone
	// The spawn logic is 2D, so Extent.Z value does not matter
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Hide and Seek", meta = (AllowPrivateAccess = "true"))
	UBoxComponent* SpawnArea;

protected: // Variables

	// Size of the grid in tiles couunt (widht and heighht)
	FIntPoint GridSize;

	// Size of a single tile
	FVector TileSize;

	// Tile origins
	TArray<FVector> SpawnTiles;

	// Tile indexes not occupied with objects
	TArray<int32> AvailableTiles;

	// Tile indexes with spawned enemies
	TArray<int32> EnemiesTiles;

	// Index of the tile where player is spawned
	int32 PlayerTileIndex;

protected: // Cache

	UPROPERTY()
	APawn* Player = nullptr;

};
