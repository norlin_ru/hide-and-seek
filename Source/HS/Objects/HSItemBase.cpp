// Fill out your copyright notice in the Description page of Project Settings.

#include "HSItemBase.h"

AHSItemBase::AHSItemBase()
{
	PrimaryActorTick.bCanEverTick = false;

	bWasThrown = false;
	bItemTaken = false;
}

void AHSItemBase::BeginPlay()
{
	Super::BeginPlay();
	
	UpdateHomeLocation();
}

void AHSItemBase::UpdateHomeLocation()
{
	HomeLocation = GetActorLocation();
}

FVector AHSItemBase::GetHomeLocation() const
{
	return HomeLocation;
}

void AHSItemBase::SetItemFocused(bool bNewFocused)
{
	OnSetFocus(bNewFocused);
}

void AHSItemBase::SetItemTaken(bool bNewTaken)
{
	bItemTaken = bNewTaken;
	OnSetTaken(bNewTaken);
}

bool AHSItemBase::IsItemTaken() const {
	return bItemTaken;
}

void AHSItemBase::ThrowItem(const FVector& Direction, float Force)
{
	SetItemTaken(false);
	OnThrowItem(Direction, Force);
}
