// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "HSItemBase.generated.h"

UCLASS()
class HS_API AHSItemBase : public AActor
{
	GENERATED_BODY()

public: // AActor

	AHSItemBase();

protected: // AActor

	virtual void BeginPlay() override;

public: // Methods

	// Make the item throw itself
	UFUNCTION(BlueprintCallable, Category = "Hide and Seek|Items")
	void ThrowItem(const FVector& Direction, float Force);

	// Update the home location for the item
	UFUNCTION(BlueprintCallable, Category = "Hide and Seek|Items")
	void UpdateHomeLocation();

	// Return the home location
	UFUNCTION(BlueprintCallable, Category = "Hide and Seek|Items")
	FVector GetHomeLocation() const;

	// Set item focused state on or off
	UFUNCTION(BlueprintCallable, Category = "Hide and Seek|Items")
	void SetItemFocused(bool bNewFocused);

	// Set item taken state on or off
	UFUNCTION(BlueprintCallable, Category = "Hide and Seek|Items")
	void SetItemTaken(bool bNewTaken);

	// Returen true if item is taken
	UFUNCTION(BlueprintCallable, Category = "Hide and Seek|Items")
	bool IsItemTaken() const;

protected: // Internal Methods

	// Triggered when the focused state was changed
	UFUNCTION(BlueprintImplementableEvent, Category = "Hide and Seek|Items")
	void OnSetFocus(bool bNewFocused);

	// Triggered when the taken state was changed
	UFUNCTION(BlueprintImplementableEvent, Category = "Hide and Seek|Items")
	void OnSetTaken(bool bNewTaken);

	// Triggered on item throw
	UFUNCTION(BlueprintImplementableEvent, Category = "Hide and Seek|Items")
	void OnThrowItem(const FVector& Direction, float Force);

protected: // Variables

	FVector HomeLocation;
	bool bItemTaken;
	bool bWasThrown;

};
