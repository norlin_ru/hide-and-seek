// Fill out your copyright notice in the Description page of Project Settings.

#include "HSSpawner.h"
#include "HS/Core/HSCharacter.h"
#include "HS/Core/HSEventsSubsystem.h"
#include "HS/Objects/HSItemBase.h"
#include "HS/Utils/HSLog.h"

#include "Components/BoxComponent.h"
#include "GameFramework/Pawn.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetArrayLibrary.h"

#include "DrawDebugHelpers.h"

AHSSpawner::AHSSpawner()
{
	PrimaryActorTick.bCanEverTick = false;
	
	SpawnArea = CreateDefaultSubobject<UBoxComponent>(FName("SpawnArea"));
	RootComponent = SpawnArea;

	EnemiesAmount = 6;
	ObstaclesAmount = 24;
	ItemsAmount = 24;

	SafeZoneRadius = 2100.f;
}

void AHSSpawner::BeginPlay()
{
	Super::BeginPlay();

	Player = UGameplayStatics::GetPlayerPawn(this, 0);

	PrepareSpawnTiles();
	
	SpawnEnemies();
	SpawnObstacles();
	SpawnItems();

	UHSEventsSubsystem* EventSubsystem = GetWorld()->GetSubsystem<UHSEventsSubsystem>();
	EventSubsystem->StartGame();
}

void AHSSpawner::PrepareSpawnTiles()
{
	const FVector Origin = SpawnArea->GetComponentLocation();
	const FVector Extent = SpawnArea->GetScaledBoxExtent();
	const FVector Area = Extent * 2.f;

	const int32 RequiredTileCount = EnemiesAmount + ObstaclesAmount + ItemsAmount;

	// Calculate the grid dimensions to fit all the required items and close to the area's aspect ratio

	GridSize = FIntPoint(0);
	GridSize.X = FMath::CeilToInt(FMath::Sqrt((float)RequiredTileCount * Area.X / Area.Y));
	GridSize.Y = FMath::CeilToInt((float)GridSize.X * Area.Y / Area.X);

	TileSize = FVector(Area.X / GridSize.X, Area.Y / GridSize.Y, 0.f);
	const int32 TilesCount = GridSize.X * GridSize.Y;

	// HS_DEBUG("Grid size: %d x %d (tiles: %d)", GridSize.X, GridSize.Y, TilesCount);
	// HS_DEBUG("Tile size: %f x %f", TileSize.X, TileSize.Y);

	const FVector HalfSize = TileSize / 2.f;

	SpawnTiles.Reserve(TilesCount);
	AvailableTiles.Reserve(TilesCount);
	
	PlayerTileIndex = FindTileIndex(Player->GetActorLocation());

	for (int32 TileX = 0; TileX < GridSize.X; TileX++)
	{
		for (int32 TileY = 0; TileY < GridSize.Y; TileY++)
		{
			const FVector TileOrigin = Origin - Extent + FVector(TileX * TileSize.X, TileY * TileSize.Y, 0.f) + HalfSize;
			
			const int32 Index = SpawnTiles.Add(TileOrigin);

			// Player tile is red
			// Safe zoin tiles is yellow
			// Regular tiles are white
			// FColor Color = Index == PlayerTileIndex ? FColor::Red : (IsSafeZone(Index) ? FColor::Yellow : FColor::White);
			// DrawDebugSphere(GetWorld(), TileOrigin, 250.f, 8, Color, true, 10000.f, 0, 3.f);

			if (Index == PlayerTileIndex)
			{
				// Don't allow to spawn anything on the player's tile
				continue;
			}

			AvailableTiles.Add(Index);
		}
	}

	const int32 AvailableTilesCount = AvailableTiles.Num();

	// Shuffle the available tiles
	for (int32 OldIndex = 0; OldIndex < AvailableTilesCount; OldIndex += 1)
	{
		const int32 Index = FMath::RandRange(OldIndex, AvailableTilesCount - 1);

		if (OldIndex == Index)
		{
			continue;
		}

		AvailableTiles.Swap(OldIndex, Index);
	}
}

int32 AHSSpawner::FindTileIndex(const FVector& Location) const
{
	const FVector Extent = SpawnArea->GetScaledBoxExtent();
	const FVector Origin = SpawnArea->GetComponentLocation();

	// Align the Location with the grid
	const FVector AlignedLocation = Location - Origin + Extent;
	const FVector ApproximateTile = AlignedLocation / TileSize;

	FIntPoint TileCoordinates = FIntPoint(FMath::FloorToInt(ApproximateTile.X), FMath::FloorToInt(ApproximateTile.Y));

	// Calculate the tile index
	return TileCoordinates.X * GridSize.Y + TileCoordinates.Y;
}

bool AHSSpawner::IsSafeZone(int32 TileIndex) const
{
	if (TileIndex == PlayerTileIndex)
	{
		return true;
	}

	const int32 Rows = GridSize.X;
	const int32 Cols = GridSize.Y;

	const float Fraction = (float)PlayerTileIndex / (float)Cols;

	const FIntPoint SafeTile(FMath::CeilToInt(Fraction), PlayerTileIndex % Cols);

	// Look for closest adjacent tiles
	TArray<FIntPoint> AdjacentMatrix{
		FIntPoint(-1, -1), FIntPoint(-1, 0), FIntPoint(-1, 1),
		FIntPoint( 0, -1),                   FIntPoint( 0, 1),
		FIntPoint( 1, -1), FIntPoint( 1, 0), FIntPoint( 1, 1),
	};

	for (const FIntPoint Offset : AdjacentMatrix)
	{
		const FIntPoint AdjacentTile = SafeTile + Offset;

		if (
			AdjacentTile.X < 0 || AdjacentTile.X >= Rows ||
			AdjacentTile.Y < 0 || AdjacentTile.Y >= Cols
		)
		{
			// Went outside of the grid, skip
			continue;
		}

		// Check if the adjacent tile is the one we're checking against
		const int32 AdjacentIndex = AdjacentTile.X * GridSize.Y + AdjacentTile.Y;
		if (TileIndex == AdjacentIndex)
		{
			return true;
		}
	}

	// Not a safe-zone tile
	return false;
}

bool AHSSpawner::GetRandomLocation(const FVector& Extent, FVector& OutLocation, bool bRespectSafeZone)
{
	const int32 TilesLeft = AvailableTiles.Num();
	if (TilesLeft == 0)
	{
		// All tiles are occupied - should not happen
		return false;
	}

	int32 ResultingTileIndex = INDEX_NONE;
	int32 Index = 0;

	const bool bHasDesiredLocation = OutLocation != FVector::ZeroVector;
	if (bHasDesiredLocation)
	{
		// Check if we can spawn at desired location

		ResultingTileIndex = FindTileIndex(OutLocation);

		Index = AvailableTiles.Find(ResultingTileIndex);
		if (Index == INDEX_NONE)
		{
			// HS_DEBUG("Can't detect an available tile for spawning at specific location!");
			return false;
		}
	} else if (bRespectSafeZone)
	{
		// Find tiles outside of the safe zone

		while (Index < TilesLeft)
		{
			const int32 TileIndex = AvailableTiles[Index];

			if (IsSafeZone(TileIndex))
			{
				Index++;
				continue;
			}

			ResultingTileIndex = TileIndex;
			break;
		}

		if (Index == TilesLeft)
		{
			return false;
		}
	} else {
		// Get any available tile

		ResultingTileIndex = AvailableTiles[Index];
	}

	AvailableTiles.RemoveAt(Index);

	const FVector TileOrigin = SpawnTiles[ResultingTileIndex];

	// Try to adjust the spawn location to fit the Extent into the tile limits
	const FVector Extent2D = FVector(Extent.X, Extent.Y, 0.f);
	const FVector BoxMin = TileOrigin - (TileSize / 2) + Extent;
	const FVector BoxMax = TileOrigin + (TileSize / 2) - Extent;

	FVector VolumePoint;
	if (bHasDesiredLocation)
	{
		// Use the desired location clamped to the tile limits
		VolumePoint = OutLocation.BoundToBox(BoxMin, BoxMax);
	} else {
		// Use the random location inside the tile
		VolumePoint = FMath::RandPointInBox(FBox(BoxMin, BoxMax));
	}
	
	// set Z to put the object right on the floor
	// Assuming the floor is always at 0.f by Z axis
	// To work with non-flat floors, will need to add a line trace to detect the actual floor surface
	VolumePoint.Z = Extent.Z;

	OutLocation = VolumePoint;

	return true;
}

AActor* AHSSpawner::SpawnActor(TSubclassOf<AActor> ObjectClass, bool bRespectSafeZone, const FVector& DesiredLocation)
{
	FActorSpawnParameters Params;
	Params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	FTransform Transform = FTransform::Identity;
	// At first, spawn the new actor in some place outside of the level
	// Required to detect the actor's extent
	Transform.SetTranslation(FVector(0.f, 0.f, -1000.f));

	// First, spawn the new object somewhere to get it's extents
	AActor* NewActor = GetWorld()->SpawnActor<AActor>(ObjectClass, Transform);
	if (!NewActor)
	{
		HS_ERROR("Can't spawn actor!");
		return nullptr;
	}

	FVector Origin;
	FVector Extent;
	NewActor->GetActorBounds(true, Origin, Extent);

	// Second, find a randomized location with respect to object's extents
	FVector Location = DesiredLocation;
	if (!GetRandomLocation(Extent, Location, bRespectSafeZone))
	{
		// Don't have any more place to spawn
		return nullptr;
	}

	// Get a random rotation
	FRotator Rotation = FRotator(0.f, FMath::FRand() * 360.f, 0.f);

	// Third, move the actor at the randomized position
	Transform.SetTranslation(Location);
	Transform.SetRotation(Rotation.Quaternion());
	NewActor->SetActorTransform(Transform);

	return NewActor;
}

void AHSSpawner::SpawnEnemies()
{
	if (!IsValid(EnemyClass))
	{
		HS_ERROR("Please setup a valid EnemyClass!");
		return;
	}

	for (int32 Index = 0; Index < EnemiesAmount; Index++)
	{
		AHSCharacter* NewEnemy = Cast<AHSCharacter>(SpawnActor(EnemyClass, true));
		if (!NewEnemy)
		{
			continue;
		}

		// Update the enemy home location after it's fully spawned
		NewEnemy->UpdateHomePosition();
	}
}

void AHSSpawner::SpawnObstacles()
{
	if (!IsValid(ObstacleClass))
	{
		HS_ERROR("Please setup a valid ObstacleClass!");
		return;
	}

	int32 Index = 0;

	TArray<AActor*> Enemies;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), EnemyClass, Enemies);

	const FVector PlayerLocation = Player->GetActorLocation();

	// Try to protect player from the nearby enemies
	// The solution is not working perfectly if we have too much enemies
	for (AActor* EnemyActor : Enemies)
	{
		const FVector Location = EnemyActor->GetActorLocation();

		const float Distance = FVector::Dist2D(PlayerLocation, Location);
		if (Distance > SafeZoneRadius)
		{
			// Too far, it's safe already
			continue;
		}

		const FVector Direction = (Location - PlayerLocation).GetSafeNormal2D();
		const FVector ObstacleLocation = PlayerLocation + Direction * (Distance / 2);

		AActor* NewObstacle = SpawnActor(ObstacleClass, false, ObstacleLocation);
		if (!NewObstacle)
		{
			continue;
		}

		Index++;
	}

	// Spawn obstacles in the level (how much left after protecting the player)
	for (; Index < ObstaclesAmount; Index++)
	{
		SpawnActor(ObstacleClass);
	}
}

void AHSSpawner::SpawnItems()
{
	if (!IsValid(ItemClass))
	{
		HS_ERROR("Please setup a valid ItemClass!");
		return;
	}

	for (int32 Index = 0; Index < ItemsAmount; Index++)
	{
		AHSItemBase* NewItem = Cast<AHSItemBase>(SpawnActor(ItemClass));
		if (!NewItem)
		{
			continue;
		}

		// Update the item's home location after it's fully spawned
		NewItem->UpdateHomeLocation();
	}
}
