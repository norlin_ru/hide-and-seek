// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HSGameModeBase.generated.h"

/**
 * Basic game mode for Hide and Seek prototype
 */
UCLASS()
class HS_API AHSGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public: // AGameModeBase

	AHSGameModeBase(const FObjectInitializer& ObjectInitializer);

	virtual void StartPlay() override;
	virtual void Tick(float DeltaTime) override;

	// Execute immediate game restart
	UFUNCTION(BlueprintCallable, Category = "Hide and Seek|Debug")
	void RestartLevel();

protected: // Internal methods

	UFUNCTION()
	void HandleGameOver();

	UFUNCTION()
	void HandleGameWin();

protected: // Settings

	// Seconds before game restart after game over / win
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hide and Seek")
	float GameRestartTimeout = 3.f;

protected: // Variables

	float RestartTimeout = 0.f;

};
