// Fill out your copyright notice in the Description page of Project Settings.

#include "HSEventsSubsystem.h"

void UHSEventsSubsystem::RegisterNoiseEvent(const FVector& Origin, AActor* Source)
{
	OnNoise.Broadcast(Origin, Source);
}

void UHSEventsSubsystem::StartGame()
{
	bGameStarted = true;
	OnGameStarted.Broadcast();
}

void UHSEventsSubsystem::RestartGame()
{
	OnGameRestart.Broadcast();
}

bool UHSEventsSubsystem::IsGameStarted()
{
	return bGameStarted;
}

void UHSEventsSubsystem::GameOver()
{
	OnGameOver.Broadcast();
}

void UHSEventsSubsystem::Win()
{
	OnWin.Broadcast();
}
