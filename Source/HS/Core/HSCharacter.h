// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "HS/Core/HSTypes.h"
#include "HSCharacter.generated.h"

class UNavigationSystemV1;
class UProceduralMeshComponent;

UCLASS()
class HS_API AHSCharacter : public ACharacter
{
	GENERATED_BODY()

public: // ACharacter

	AHSCharacter();

protected: // ACharacter

	virtual void PostInitializeComponents() override;
	virtual void BeginPlay() override;

public: // Methods

	// Set the new home position from the current actor's location and rotation
	UFUNCTION(BlueprintCallable, Category = "Hide and Seek|Items")
	void UpdateHomePosition();

	// Return the initial actor's location
	UFUNCTION(BlueprintCallable, Category = "Hide and Seek|Items")
	FVector GetHomeLocation() const;

	// Return the initial actor's rotation
	UFUNCTION(BlueprintCallable, Category = "Hide and Seek|Items")
	FRotator GetHomeRotation() const;

	// Return true if this actor is a target for bots
	UFUNCTION(BlueprintCallable, Category = "Hide and Seek|Items")
	bool IsTarget() const;

	// Update the FOV Display mesh from the traced vertices
	void UpdateFOVDisplay(const TArray<FVector>& Vertices, const TArray<int32>& Triangles, const TArray<FColor>& VertexColors);

	// Set the movement mode for AI bots - depending on the AI state
	UFUNCTION(BlueprintCallable, Category = "Hide and Seek")
	void SetMovementType(EHSMovementType Type);

	// A helper method to find a location on dynamic navmesh when the Point is outside of the current navmesh
	bool FindNextNavigationPoint(const FVector& Point, FVector& OutLocation) const;

protected: // Settings

	// If true, bots will chase this actor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hide and Seek")
	bool bIsTarget = false;

	// Material for the FOVDisplay procedural mesh
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hide and Seek")
	UMaterialInterface* FOVDisplayMaterial = nullptr;

	// Radius for navigation tiles generation
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hide and Seek")
	float TileGenerationRadius;

	// Radius for navigation tiles removal
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hide and Seek")
	float TileRemovalRadius;

	// Character default speed
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hide and Seek")
	float Speed;

	// Speed multipliers for different modes
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hide and Seek")
	TMap<EHSMovementType, float> SpeedMultiplier;

private: // Components

	// The component is used to display Field-of-View for AI-controller bots
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Hide and Seek", meta = (AllowPrivateAccess = "true"))
	UProceduralMeshComponent* FOVDisplayMesh;

protected: // Variables

	FVector HomeLocation;
	FRotator HomeRotation;

protected: // Cache

	UPROPERTY()
	UNavigationSystemV1* NavigationSystem = nullptr;

};
