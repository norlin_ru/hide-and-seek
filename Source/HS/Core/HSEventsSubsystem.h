// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/WorldSubsystem.h"
#include "HSEventsSubsystem.generated.h"

/**
 * A subsystem to handle world global events
 */
UCLASS()
class HS_API UHSEventsSubsystem : public UWorldSubsystem
{
	GENERATED_BODY()
	
	DECLARE_DYNAMIC_MULTICAST_DELEGATE(FHSDelegateGameStateChanged);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FHSDelegateEventNoise, FVector, Origin, AActor*, Source);

public: //Delegates

	// Triggered when Noise produced
	UPROPERTY(BlueprintAssignable, Category = "Hide and Seek|Events")
	FHSDelegateEventNoise OnNoise;

	// Triggered when all the spawn logic is completed and actual gameplay is started
	UPROPERTY(BlueprintAssignable, Category = "Hide and Seek|Events")
	FHSDelegateGameStateChanged OnGameStarted;

	// Triggered when game restart is requested
	UPROPERTY(BlueprintAssignable, Category = "Hide and Seek|Events")
	FHSDelegateGameStateChanged OnGameRestart;

	// Triggered when player loses the game
	UPROPERTY(BlueprintAssignable, Category = "Hide and Seek|Events")
	FHSDelegateGameStateChanged OnGameOver;

	// Triggered when player wins the game
	UPROPERTY(BlueprintAssignable, Category = "Hide and Seek|Events")
	FHSDelegateGameStateChanged OnWin;

public: // Methods

	// Produce Noise event
	UFUNCTION(BlueprintCallable, Category = "Hide and Seek|Events")
	void RegisterNoiseEvent(const FVector& Origin, AActor* Source);

	// Start actual gameplay
	// Should only be called form the HSSpawner when all the objects are spawned
	UFUNCTION(BlueprintCallable, Category = "Hide and Seek|Events")
	void StartGame();

	// Request immediate game restart
	UFUNCTION(BlueprintCallable, Category = "Hide and Seek|Events")
	void RestartGame();

	// Check if the gameplay was started
	UFUNCTION(BlueprintCallable, Category = "Hide and Seek|Events")
	bool IsGameStarted();

	// Trigger Game Over state
	UFUNCTION(BlueprintCallable, Category = "Hide and Seek|Events")
	void GameOver();

	// Trigger Win state
	UFUNCTION(BlueprintCallable, Category = "Hide and Seek|Events")
	void Win();

protected: // Variables

	bool bGameStarted = false;

};
