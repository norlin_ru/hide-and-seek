// Fill out your copyright notice in the Description page of Project Settings.

#include "HSGameModeBase.h"
#include "HS/Core/HSCharacter.h"
#include "HS/Core/HSEventsSubsystem.h"
#include "HS/Player/HSPlayerController.h"
#include "HS/Utils/HSLog.h"

#include "Kismet/GameplayStatics.h"

AHSGameModeBase::AHSGameModeBase(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bTickEvenWhenPaused = true;
	
	PlayerControllerClass = AHSPlayerController::StaticClass();
	DefaultPawnClass = AHSCharacter::StaticClass();
}

void AHSGameModeBase::StartPlay()
{
	UHSEventsSubsystem* EventSubsystem = GetWorld()->GetSubsystem<UHSEventsSubsystem>();
	EventSubsystem->OnGameRestart.AddDynamic(this, &AHSGameModeBase::RestartLevel);
	EventSubsystem->OnGameOver.AddDynamic(this, &AHSGameModeBase::HandleGameOver);
	EventSubsystem->OnWin.AddDynamic(this, &AHSGameModeBase::HandleGameWin);

	Super::StartPlay();
}

void AHSGameModeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (RestartTimeout <= 0)
	{
		return;
	}

	// Delay before game restart after Win/GameOver
	RestartTimeout -= DeltaTime;
	if (RestartTimeout <= 0)
	{
		RestartLevel();
	}
}

void AHSGameModeBase::HandleGameOver()
{
	HS_LOG("Game Over!");

	SetPause(UGameplayStatics::GetPlayerController(this, 0));
	RestartTimeout = GameRestartTimeout;
}

void AHSGameModeBase::HandleGameWin()
{
	HS_LOG("You win!");

	SetPause(UGameplayStatics::GetPlayerController(this, 0));
	RestartTimeout = GameRestartTimeout;
}

void AHSGameModeBase::RestartLevel()
{
	RestartTimeout = -1.f;

	HS_LOG("Restarting level...");

	// Restart current level
	const FString CurrentLevel = UGameplayStatics::GetCurrentLevelName(this);
	UGameplayStatics::OpenLevel(GetWorld(), FName(*CurrentLevel), true, TEXT(""));
}
