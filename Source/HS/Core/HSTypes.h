// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HSTypes.generated.h"

class AHSCharacter;
class AHSItemBase;

// All the possible states for the AI
UENUM(BlueprintType)
enum class EHSAIState : uint8 {
	None = 0 UMETA(Tooltip = "Don't have a state, choose a new one"),
	Idle UMETA(Tooltip = "Idle state, do nothing"),
	LookAround UMETA(Tooltip = "Looking around"),
	BackToHome UMETA(Tooltip = "Going back to home"),
	DetectNoise UMETA(Tooltip = "AI has detected some noise"),
	DetectTarget UMETA(Tooltip = "Target is detected!"),
	FindMisplacedItem UMETA(Tooltip = "Search for a closest misplaced item"),
	TakeItem UMETA(Tooltip = "Take the item"),
	ReturnItem UMETA(Tooltip = "Bring the item back to it's home location"),
	GameOver UMETA(Tooltip = "Target is caught!"),
};

// Different movement types for bots
UENUM(BlueprintType)
enum class EHSMovementType : uint8 {
	Walk = 0,
	Run,
	Chase
};

// Types of possible signals detected by bots
UENUM(BlueprintType)
enum class EHSSignalType : uint8 {
	None = 0 UMETA(Tooltip = "No signal"),
	Noise UMETA(Tooltip = "Noise detected"),
	Target UMETA(Tooltip = "Target spotted"),
	MisplacedItem UMETA(Tooltip = "Found a misplaced item")
};

// Signal for AI detection
USTRUCT(BlueprintType)
struct FHSSignal {
	GENERATED_BODY()

	// Type of the signal
	UPROPERTY(BlueprintReadWrite, Category = "Hide and Seek|Signal")
	EHSSignalType Type;

	// Detected location of the signal
	UPROPERTY(BlueprintReadWrite, Category = "Hide and Seek|Signal")
	FVector Location;

	// Detected target, if any
	UPROPERTY(BlueprintReadWrite, Category = "Hide and Seek|Signal")
	AHSCharacter* Target;

	// Detected item, if any
	UPROPERTY(BlueprintReadWrite, Category = "Hide and Seek|Signal")
	AHSItemBase* TargetItem;

	// Create an empty or typed signal with no payload
	FHSSignal(EHSSignalType InType = EHSSignalType::None)
		: Type(InType), Location(FVector::ZeroVector), Target(nullptr), TargetItem(nullptr)
	{}

	// Create Noise signal
	FHSSignal(FVector InLocation)
		: Type(EHSSignalType::Noise), Location(InLocation), Target(nullptr), TargetItem(nullptr)
	{}

	// Create Target signal
	FHSSignal(AHSCharacter* InTarget);

	// Create MisplacedItem signal
	FHSSignal(AHSItemBase* InTargetItem);

	// An empty signal
	static const FHSSignal None;

	bool operator==(const FHSSignal& Other) const {
		return
			Type == Other.Type && 
			Location == Other.Location &&
			Target == Other.Target &&
			TargetItem == Other.TargetItem;
	}

	bool operator!=(const FHSSignal& Other) const {
		return !(*this == Other);
	}

};

