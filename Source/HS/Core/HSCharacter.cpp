// Fill out your copyright notice in the Description page of Project Settings.

#include "HSCharacter.h"
#include "HS/Utils/HSLog.h"

#include "Components/CapsuleComponent.h"
#include "Engine/CollisionProfile.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "NavigationSystem.h"
#include "ProceduralMeshComponent.h"

AHSCharacter::AHSCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	// Use 50-cm (radius & half-height) capsule for movement (the default pawn is 1m-height cylinder)
	GetCapsuleComponent()->InitCapsuleSize(50.0f, 50.0f);

	TileGenerationRadius = 3000.f;
	TileRemovalRadius = 3000.f;

	Speed = 600.f;
}

void AHSCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	// Setup the FOV into the world: it's flickering when attached to Character when the Character is moving, because of the async cooking
	FOVDisplayMesh = NewObject<UProceduralMeshComponent>(this);
	FOVDisplayMesh->SetWorldTransform(FTransform::Identity);
	FOVDisplayMesh->bUseAsyncCooking = true;
	FOVDisplayMesh->SetMaterial(0, FOVDisplayMaterial);

	// Does not have any collision data
	FOVDisplayMesh->ContainsPhysicsTriMeshData(false);
	FOVDisplayMesh->SetCollisionProfileName(UCollisionProfile::NoCollision_ProfileName);

	FOVDisplayMesh->RegisterComponentWithWorld(GetWorld());
}

void AHSCharacter::BeginPlay()
{
	Super::BeginPlay();

	// Set the default movement to Walk
	SetMovementType(EHSMovementType::Walk);

	NavigationSystem = UNavigationSystemV1::GetNavigationSystem(this);
	if (!NavigationSystem) {
		HS_ERROR("Can't find Navigation System for %s", *GetName());
		return;
	}

	// Register the character as nav invoker - for dynamic navmesh
	NavigationSystem->RegisterNavigationInvoker(this, TileGenerationRadius, TileRemovalRadius);

	UpdateHomePosition();
}

void AHSCharacter::UpdateHomePosition()
{
	HomeLocation = GetActorLocation();
	HomeRotation = GetActorRotation();
}

FVector AHSCharacter::GetHomeLocation() const
{
	return HomeLocation;
}

FRotator AHSCharacter::GetHomeRotation() const
{
	return HomeRotation;
}

bool AHSCharacter::IsTarget() const
{
	return bIsTarget;
}

void AHSCharacter::UpdateFOVDisplay(const TArray<FVector>& Vertices, const TArray<int32>& Triangles, const TArray<FColor>& VertexColors)
{
	TArray<FVector> Normals;
	TArray<FVector2D> UVs;
	TArray<FProcMeshTangent> Tangents;

	// Using only Vertices, Triangles and VertexColors
	FOVDisplayMesh->CreateMeshSection(0, Vertices, Triangles, Normals, UVs, VertexColors, Tangents, false);
}

void AHSCharacter::SetMovementType(EHSMovementType Type)
{
	float NewSpeed = Speed;

	// Find and apply the movement mode speed multiplier
	const float* CurrentMultiplier = SpeedMultiplier.Find(Type);
	if (CurrentMultiplier)
	{
		NewSpeed = NewSpeed * (*CurrentMultiplier);
	}

	GetCharacterMovement()->MaxWalkSpeed = NewSpeed;
}

bool AHSCharacter::FindNextNavigationPoint(const FVector& Point, FVector& OutLocation) const
{
	const float Distance = FVector::Dist2D(GetActorLocation(), Point);

	// Assuming the point should be on the navmesh
	if (Distance < TileGenerationRadius)
	{
		OutLocation = Point;
		return true;
	}

	// If the point is outside of the mesh - try to find an appropriate location for movement on existing navmesh
	FNavLocation NavLocation;
	const bool bSucceed = NavigationSystem->ProjectPointToNavigation(Point, NavLocation, FVector(Distance));
	if (!bSucceed)
	{
		return false;
	}

	OutLocation = NavLocation.Location;
	return true;
}
