// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "HSPlayerController.generated.h"

class APawn;

class AHSCharacter;
class AHSItemBase;

/**
 * Player controller for Hide and Seek prototype
 */
UCLASS()
class HS_API AHSPlayerController : public APlayerController
{
	GENERATED_BODY()

public:

	AHSPlayerController(const FObjectInitializer& ObjectInitializer);

	virtual void PlayerTick(float DeltaTime);

protected: // APlayerController

	virtual void BeginPlay() override;

	virtual void OnPossess(APawn* InPawn) override;
	virtual void OnUnPossess() override;

	virtual void SetupInputComponent() override;
	virtual void ProcessPlayerInput(const float DeltaTime, const bool bGamePaused);

protected: // Internal methods

	// Try to find a pickable item
	void FindItemsInReach();

	// Update the carried item location
	void UpdateCarriedItem();

	// Toggle the game's pause
	UFUNCTION(BlueprintCallable, Category = "Hide and seek")
	void TogglePause();

	// Handle item interaction (begin)
	UFUNCTION(BlueprintCallable, Category = "Hide and seek")
	void OnItemInteract();

	// Handle item interaction (done)
	UFUNCTION(BlueprintCallable, Category = "Hide and seek")
	void OnItemInteractionDone();

	// Return the current throw charge [0.f .. 1.f]
	UFUNCTION(BlueprintCallable, Category = "Hide and seek")
	float GetChargePercent() const;

protected: // Settings

	// Maximum distance at which player can grab an item
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hide and Seek|Items", meta = (ClampMin = 1))
	float ReachDistance;

	// Reach radius
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hide and Seek|Items", meta = (ClampMin = 1))
	float ReachRadius;

	// Carried item position offset
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hide and Seek|Items")
	FVector CarriedItemOffset;

	// Offset where the item goes while charging throw power
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hide and Seek|Items")
	FVector ThrowChargeOffset;

	// If true - the carried item will be thrown in the direction of player camera look
	// If false - the direction's Pitch of the throw will be fixed at ThrowPitchAngle angle
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hide and Seek|Items")
	bool bThrowAtLookDirection;

	// Fixed Pitch angle for item throws
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hide and Seek|Items", meta = ( EditCondition = "!bThrowAtLookDirection" ))
	float ThrowPitchAngle;

	// Min throw power
	// Roughly represents the throw distance ~ ThrowPowerMax / 10 meters
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hide and Seek|Items", meta = (ClampMin = 0))
	float ThrowPowerMin;

	// Max throw power
	// Roughly represents the throw distance ~ ThrowPowerMax / 10 meters
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hide and Seek|Items", meta = (ClampMin = 0))
	float ThrowPowerMax;

	// Duration for collecting the power from 0 to max
	// How long player should hold the throw button to charge at ThrowPowerMax power
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hide and Seek|Items", meta = (ClampMin = 0))
	float ThrowPowerDuration;

	// Control bindings names will not be changed often, probably - don't expose them as UPROPERTY's
	static const FName BindingNameAxisMoveForward;
	static const FName BindingNameAxisMoveRight;
	static const FName BindingNameAxisAimUp;
	static const FName BindingNameAxisAimRight;
	static const FName BindingNameKeyPause;

protected: // Variables

	// Current focused or carried item
	UPROPERTY(BlueprintReadOnly, Category = "Hide and Seek|Items")
	AHSItemBase* FocusedItem = nullptr;

	// True if player is carrying an item
	UPROPERTY(BlueprintReadOnly, Category = "Hide and Seek|Items")
	bool bCarryingItem = false;

	// true if item is picked up
	UPROPERTY(BlueprintReadOnly, Category = "Hide and Seek|Items")
	bool bHasItem = false;

	// The duration of current throw charge
	UPROPERTY(BlueprintReadOnly, Category = "Hide and Seek|Items")
	float ThrowChargeTime = 0.f;

	// true if charging
	UPROPERTY(BlueprintReadOnly, Category = "Hide and Seek|Items")
	bool bThrowCharging = false;

protected: // Cache

	UPROPERTY(BlueprintReadOnly, Category = "Hide and Seek")
	AHSCharacter* PlayerCharacter = nullptr;

};
