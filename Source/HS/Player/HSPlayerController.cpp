// Fill out your copyright notice in the Description page of Project Settings.

#include "HSPlayerController.h"
#include "HS/Core/HSCharacter.h"
#include "HS/Objects/HSItemBase.h"
#include "HS/Utils/HSLog.h"

#include "GameFramework/CharacterMovementComponent.h"
#include "DrawDebugHelpers.h"

const FName AHSPlayerController::BindingNameAxisMoveForward("MoveForward");
const FName AHSPlayerController::BindingNameAxisMoveRight("MoveRight");
const FName AHSPlayerController::BindingNameAxisAimUp("AimUp");
const FName AHSPlayerController::BindingNameAxisAimRight("AimRight");
const FName AHSPlayerController::BindingNameKeyPause("Pause");

AHSPlayerController::AHSPlayerController(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bShowMouseCursor = false;

	ReachDistance = 100.f;
	ReachRadius = 20.f;

	CarriedItemOffset = FVector(100.f, 30.f, -20.f);
	ThrowChargeOffset = FVector(-40.f, 30.f, 40.f);

	bThrowAtLookDirection = false;
	ThrowPitchAngle = 20.f;
	ThrowPowerMin = 100.f;
	ThrowPowerMax = 1500.f;
	ThrowPowerDuration = 2.f;
}

void AHSPlayerController::BeginPlay()
{
	Super::BeginPlay();

	// Set input mode to game only - we don't need a cursor in this prototype
	FInputModeGameOnly InputMode;
	SetInputMode(InputMode);
}

void AHSPlayerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	// Save the pointer to our character
	PlayerCharacter = Cast<AHSCharacter>(InPawn);
}

void AHSPlayerController::OnUnPossess()
{
	Super::OnUnPossess();

	PlayerCharacter = nullptr;
}

void AHSPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	// Setup move & look axis

	InputComponent->BindAxis(BindingNameAxisMoveForward);
	InputComponent->BindAxis(BindingNameAxisMoveRight);

	InputComponent->BindAxis(BindingNameAxisAimUp);
	InputComponent->BindAxis(BindingNameAxisAimRight);

	// Setup key bindings

	// Allow to unpause when paused
	FInputActionBinding BindingKeyPause(BindingNameKeyPause, IE_Pressed);
	BindingKeyPause.bExecuteWhenPaused = true;
	BindingKeyPause.ActionDelegate.BindDelegate(this, &AHSPlayerController::TogglePause);
	InputComponent->AddActionBinding(BindingKeyPause);

	InputComponent->BindAction("TakeItem", IE_Pressed, this, &AHSPlayerController::OnItemInteract);
	InputComponent->BindAction("TakeItem", IE_Released, this, &AHSPlayerController::OnItemInteractionDone);
}

void AHSPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	if (bHasItem)
	{
		UpdateCarriedItem();
	} else {
		FindItemsInReach();
	}
}

void AHSPlayerController::ProcessPlayerInput(const float DeltaTime, const bool bGamePaused)
{
	Super::ProcessPlayerInput(DeltaTime, bGamePaused);

	if (bGamePaused)
	{
		return;
	}

	ensure(PlayerCharacter);

	// Process aim input

	const float AimUpValue = GetInputAxisValue(BindingNameAxisAimUp);
	const float AimRightValue = GetInputAxisValue(BindingNameAxisAimRight);

	// Using the controller rotation for aim
	// Assuming character has the option enabled (character bUseControllerRotationYaw == true)
	AddYawInput(AimRightValue);

	// Assuming the Pitch will only be used to rotate the camera (character bUseControllerRotationPitch == false)
	AddPitchInput(AimUpValue);

	// Process movement input

	const float MovementForwardValue = GetInputAxisValue(BindingNameAxisMoveForward);
	const float MovementRightValue = GetInputAxisValue(BindingNameAxisMoveRight);

	// Calculate input vector relative to the character
	const FVector CharacterForwardVector = PlayerCharacter->GetActorForwardVector();
	const FVector CharacterRightVector = PlayerCharacter->GetActorRightVector();

	// Don't normalize the input vector - all the acceleration / speed clamping is handled by the movement component
	// Using the raw input values allows player to choose speed when using an analogue controller for movement (e.g. gamepad stick)
	const FVector InputVector = CharacterForwardVector * MovementForwardValue + CharacterRightVector * MovementRightValue;

	PlayerCharacter->GetCharacterMovement()->AddInputVector(InputVector);
}

void AHSPlayerController::TogglePause()
{
	SetPause(!IsPaused());
}

void AHSPlayerController::OnItemInteract()
{
	if (!FocusedItem)
	{
		// No item found in reach
		return;
	}

	if (bCarryingItem)
	{
		// Start increasing throw power
		ThrowChargeTime = GetWorld()->GetTimeSeconds();
		bThrowCharging = true;
		return;
	}

	// Start picking up the item
	FocusedItem->SetItemFocused(false);
	FocusedItem->SetItemTaken(true);

	bHasItem = true;
}

void AHSPlayerController::OnItemInteractionDone()
{
	if (!FocusedItem || !bHasItem)
	{
		return;
	}

	if (!bCarryingItem)
	{
		// Pick up the item, start carrying
		bCarryingItem = true;
		return;
	}

	// Throw the item

	const float ThrowPower = FMath::Lerp(0.f, ThrowPowerMax, GetChargePercent());

	FVector Location;
	FRotator Rotation;
	GetPlayerViewPoint(Location, Rotation);

	if (!bThrowAtLookDirection)
	{
		Rotation.Pitch = ThrowPitchAngle;
	}

	const FVector ThrowDirection = Rotation.Vector();

	FocusedItem->ThrowItem(ThrowDirection, ThrowPower);

	// Reset the item's handling states
	FocusedItem = nullptr;
	bCarryingItem = false;
	bHasItem = false;
	bThrowCharging = false;
}

float AHSPlayerController::GetChargePercent() const
{
	if (!bThrowCharging)
	{
		return 0.f;
	}

	const float ThrowChargeElapsed = GetWorld()->GetTimeSeconds() - ThrowChargeTime;
	return FMath::Clamp(ThrowChargeElapsed / ThrowPowerDuration, 0.f, 1.f);
}

void AHSPlayerController::FindItemsInReach()
{
	if (!PlayerCharacter)
	{
		return;
	}

	FVector Location;
	FRotator Rotation;
	GetPlayerViewPoint(Location, Rotation);

	const FVector LookDirection = Rotation.Vector();

	// Using the distance as half-height for capsule since the capsule is placed on angle
	// while reach distance is considered to be 2d distance from player to item
	const float ReachCapsuleHalfHeight = ReachDistance;

	const FCollisionShape ReachCapsule = FCollisionShape::MakeCapsule(ReachRadius, ReachCapsuleHalfHeight);
	const FVector ReachCapsuleLocation = Location + LookDirection * (ReachCapsuleHalfHeight);

	// The capsule default orientation is vertical, so we need to rotate it by Pitch along the look direction
	const FQuat ReachCapsuleRotation = Rotation.Quaternion() * FRotator(90.f, 0.f, 0.f).Quaternion();

	static const FName ReachOverlapName(TEXT("ReachOverlap"));
	FCollisionQueryParams Params(ReachOverlapName);

	TArray<FOverlapResult> Overlaps;
	const bool bHasHits = GetWorld()->OverlapMultiByChannel(Overlaps, ReachCapsuleLocation, ReachCapsuleRotation, ECC_Visibility, ReachCapsule, Params);
	// DrawDebugCapsule(GetWorld(), ReachCapsuleLocation, ReachCapsuleHalfHeight, ReachRadius, ReachCapsuleRotation, FColor::Blue, false, 0.1f);

	float MaxDotProduct = 0.f;
	AHSItemBase* NewFocusedItem = nullptr;

	for (const FOverlapResult& Overlap : Overlaps)
	{
		AHSItemBase* Item = Cast<AHSItemBase>(Overlap.GetActor());
		if (!Item)
		{
			continue;
		}

		const FVector ItemLocation = Item->GetActorLocation();
		const float Distance2D = FVector::Dist2D(ItemLocation, Location);
		if (Distance2D > ReachDistance)
		{
			// The item is out of reach
			continue;
		}

		// Detect the item player most likely looking at

		const FVector LookToItem = (ItemLocation - Location).GetSafeNormal();
		const float ItemLookDotProduct = LookDirection | LookToItem;

		if (ItemLookDotProduct >= MaxDotProduct)
		{
			MaxDotProduct = ItemLookDotProduct;
			NewFocusedItem = Item;
		}
	}

	if (NewFocusedItem == FocusedItem)
	{
		// No focus change
		return;
	}

	// Update focus item

	if (FocusedItem)
	{
		FocusedItem->SetItemFocused(false);
	}

	FocusedItem = NewFocusedItem;

	if (FocusedItem)
	{
		FocusedItem->SetItemFocused(true);
	}
}

void AHSPlayerController::UpdateCarriedItem()
{
	if (!PlayerCharacter)
	{
		return;
	}

	// Should not happen, just in case
	if (!FocusedItem)
	{
		bCarryingItem = false;
		bHasItem = false;
		bThrowCharging = false;
		return;
	}

	FVector Location;
	FRotator Rotation;
	GetPlayerViewPoint(Location, Rotation);

	const FVector ItemOffset = Rotation.RotateVector(CarriedItemOffset);

	// Use easeOutCubic interpolation for charging visuals
	// 1 - pow(1 - x, 3);
	const float ChargeOffsetAlpha = 1 - FMath::Pow(1 - GetChargePercent(), 3);
	const FVector ChargeOffset = Rotation.RotateVector(ThrowChargeOffset * ChargeOffsetAlpha);

	const FVector NewLocation = Location + ItemOffset + ChargeOffset;

	FHitResult HitResult;
	FocusedItem->SetActorLocation(NewLocation, true, &HitResult);
	if (HitResult.bBlockingHit)
	{
		// Quick hack to move the taken object when blocked by something
		FocusedItem->SetActorLocation(Location);
		FocusedItem->SetActorLocation(NewLocation, true);
	}
}
