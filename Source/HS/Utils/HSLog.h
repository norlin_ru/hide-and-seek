// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

DECLARE_LOG_CATEGORY_EXTERN(HS, Verbose, All);

#define HS_DEBUG(Format, ...) \
	UE_LOG(HS, Warning, TEXT("%s: " Format), __FUNCTION__, ##__VA_ARGS__)

#define HS_LOG(Format, ...) \
	UE_LOG(HS, Display, TEXT("%s: " Format), __FUNCTION__, ##__VA_ARGS__)

#define HS_WARN(Format, ...) \
	UE_LOG(HS, Warning, TEXT("%s: " Format), __FUNCTION__, ##__VA_ARGS__)

#define HS_ERROR(Format, ...) \
	UE_LOG(HS, Error, TEXT("%s: " Format), __FUNCTION__, ##__VA_ARGS__)


#if WITH_EDITORONLY_DATA

#define ENUM_TO_STRING(EnumType, EnumValue) FHSEnumUtil::EnumToString<EnumType>(FString(TEXT(#EnumType)), (EnumValue))

class FHSEnumUtil {
public:

	template <typename TEnum>
	static FString EnumToString(const FString& Name, TEnum Value) {
		const UEnum* EnumPtr = FindObject<UEnum>(ANY_PACKAGE, *Name, true);
		if (!EnumPtr) {
			return FString("Invalid");
		}

		return EnumPtr->GetDisplayNameTextByIndex((int64)Value).ToString();
	}
};

#endif
