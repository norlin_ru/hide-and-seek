// Fill out your copyright notice in the Description page of Project Settings.

#include "HSAIController.h"
#include "HS/Core/HSCharacter.h"
#include "HS/Core/HSEventsSubsystem.h"
#include "HS/Objects/HSItemBase.h"
#include "HS/Utils/HSLog.h"

#include "Engine/GameInstance.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

const FHSSignal FHSSignal::None(EHSSignalType::None);

FHSSignal::FHSSignal(AHSCharacter* InTarget)
{
	if (!IsValid(InTarget))
	{
		Type = EHSSignalType::None;
		Location = FVector::ZeroVector;
		Target = nullptr;
		TargetItem = nullptr;
		return;
	}

	Type = EHSSignalType::Target;
	Location = InTarget->GetActorLocation();
	Target = InTarget;
	TargetItem = nullptr;
}

FHSSignal::FHSSignal(AHSItemBase* InTargetItem)
{
	if (!IsValid(InTargetItem))
	{
		HS_WARN("Wrong item!");

		Type = EHSSignalType::None;
		Location = FVector::ZeroVector;
		Target = nullptr;
		TargetItem = nullptr;
		return;
	}

	Type = EHSSignalType::MisplacedItem;
	Location = InTargetItem->GetActorLocation();
	Target = nullptr;
	TargetItem = InTargetItem;
}

AHSAIController::AHSAIController()
{
	PrimaryActorTick.bCanEverTick = true;

	// Default minimal target size is 1 meter
	MinimalTargetSize = 100.f;

	// Default distance is 20 meters
	FOVDistance = 20 * 100.f;
	HearDistance = 20 * 100.f;

	FOVOffset = FVector::ZeroVector;
	FOVAngle = 70.f;

	FOVDisplayOffset = FVector(0.f, 0.f, -45.f);

	ItemAcceptanceDistance = 30.f;

	// Should be calculated based on the set parameters
	MaximumStepAngle = 5.f;

	LookAroundTimeoutMin = 3.f;
	LookAroundTimeoutMax = 12.f;
	LookAroundDuration = 2.f;

	AcceptanceRadius = 10.f;

	State = EHSAIState::None;
	bCanSeeTarget = false;
}

void AHSAIController::BeginPlay()
{
	Super::BeginPlay();

	// Calculate the minimal angle to catch the target of the required size on max distance
	const float StepTan = MinimalTargetSize / FOVDistance;
	MaximumStepAngle = FMath::RadiansToDegrees(FMath::Atan(StepTan));

	// Subscribe to world events
	UHSEventsSubsystem* EventSubsystem = GetWorld()->GetSubsystem<UHSEventsSubsystem>();

	// Listen for Noise events
	EventSubsystem->OnNoise.AddDynamic(this, &AHSAIController::OnNoiseDetected);

	// Process the game started event - to check if player can be detected right on launch
	if (EventSubsystem->IsGameStarted())
	{
		OnGameStarted();
	}
	else
	{
		EventSubsystem->OnGameStarted.AddDynamic(this, &AHSAIController::OnGameStarted);
	}
}

void AHSAIController::OnGameStarted()
{
	FOVTrace(360.f);

	if (bCanSeeTarget)
	{
		// Restart level if we can detect player right on start

		// TODO: Add a safe-guard to avoid endless restart loop
		HS_LOG("OMG Player is in danger right on level start!");
		UHSEventsSubsystem* EventSubsystem = GetWorld()->GetSubsystem<UHSEventsSubsystem>();
		EventSubsystem->RestartGame();
	}
	else
	{
		ForgetSignal();
	}
}

void AHSAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	// Save the pointer to our character
	EnemyCharacter = Cast<AHSCharacter>(InPawn);
}

void AHSAIController::OnUnPossess()
{
	Super::OnUnPossess();

	EnemyCharacter = nullptr;
}

void AHSAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!IsValid(EnemyCharacter))
	{
		return;
	}

	FOVTrace(FOVAngle);

	CurrentStateDuration += DeltaTime;

	// Process the AI state machine
	EHSAIState NewState = UpdateState(DeltaTime);

	// Check if AI is transitioning to a new state
	if (NewState != State)
	{
		BeforeStateTransition(NewState);

#if WITH_EDITORONLY_DATA
		HS_LOG("State changed: %s -> %s (%s)", *ENUM_TO_STRING(EHSAIState, State), *ENUM_TO_STRING(EHSAIState, NewState), *EnemyCharacter->GetName());
#endif
		
		EHSAIState OldState = State;

		State = NewState;
		CurrentStateDuration = 0.f;

		AfterStateTransition(OldState);
	}
}

void AHSAIController::FOVTrace(float Angle)
{
	UWorld* World = GetWorld();

	// Trace parameters
	const ECollisionChannel TraceChannel = ECC_Visibility;
	const FCollisionQueryParams Params;

	// Cached location and direction
	const FVector OwnerLocation = EnemyCharacter->GetActorLocation() + FOVOffset;
	const FVector ForwardVector = EnemyCharacter->GetActorForwardVector();

	const float HalfAngle = Angle / 2;

	// Calculate the requires rays count for the Angle, to not miss the target of the required size
	const int32 RaysCount = FMath::CeilToInt(Angle / MaximumStepAngle);
	const float StepAngle = Angle / RaysCount;

	// Each trace will add a new point to the array + the starting point
	const int32 PointsCount = RaysCount + 1;

	// Prepare for saving vertices
	TArray<FVector> TracePoints;
	TracePoints.Reserve(PointsCount);
	TracePoints.Add(OwnerLocation + FOVDisplayOffset);

	// Vertex color is used to display detected targets
	TArray<FColor> VertexColors;
	VertexColors.Reserve(PointsCount);
	VertexColors.Add(FColor::Black);

	AHSCharacter* DetectedTarget = nullptr;

	// Trace FOV sector with line traces
	// Loop from max to min to place points in right order - otherwise resulting triangles will face down
	for (float RayAngle = HalfAngle; RayAngle > -HalfAngle; RayAngle -= StepAngle)
	{
		// Calculate direction based on current rotation and RayAngle
		const FVector TraceDirection = ForwardVector.RotateAngleAxis(RayAngle, FVector::UpVector);
		const FVector TraceEndLocation = OwnerLocation + TraceDirection * FOVDistance;

		// DrawDebugLine(GetWorld(), OwnerLocation, TraceEndLocation, FColor::Red, false, 0.1f, 0, 3.f);

		// Launch traces
		FHitResult Hit;
		if (!World->LineTraceSingleByChannel(Hit, OwnerLocation, TraceEndLocation, TraceChannel, Params))
		{
			// No hit - saving the trace end point for FOV Display
			TracePoints.Add(TraceEndLocation + FOVDisplayOffset);
			VertexColors.Add(FColor::Black);
			continue;
		}

		// Save the hit point as vertix for FOV Display
		TracePoints.Add(Hit.ImpactPoint + FOVDisplayOffset);

		// Check if target is detected
		AHSCharacter* CharacterInSight = Cast<AHSCharacter>(Hit.GetActor());
		if (!CharacterInSight || !CharacterInSight->IsTarget())
		{
			VertexColors.Add(FColor::Black);
			continue;
		}

		// Save the detected character
		DetectedTarget = CharacterInSight;

		// Mark vertices where target is detected
		VertexColors.Add(FColor::White);
	}

	// If target has been detected in this Tick
	bCanSeeTarget = DetectedTarget != nullptr;
	if (bCanSeeTarget)
	{
		const FHSSignal NewSignal(DetectedTarget);
		DetectSignal(NewSignal);
	}

	// Calculate FOV triangles

	// If we have a full circle, then we'll have 1 more triangle
	const bool bIsFullCircle = Angle == 360.f;
	const int32 TrianglesCount = (PointsCount - (bIsFullCircle ? 1 : 2)) * 3;

	TArray<int32> Triangles;
	Triangles.Reserve(TrianglesCount);

	// Add triangles for each section of the FOV sector
	for (int32 Index = 1; Index < PointsCount; Index++)
	{
		// First vertex is always the starting location
		Triangles.Add(0);
		Triangles.Add(Index);
		Triangles.Add(Index + 1);
	}

	// Make the full circle if we're tracing at 360 degrees
	if (bIsFullCircle)
	{
		Triangles.Add(0);

		// Last point in the circle
		const int32 FullCircleLastIndex = (PointsCount - 2) * 3;
		Triangles.Add(FullCircleLastIndex);

		// Reuse the first vertex for the last triangle if we have the full circle
		Triangles.Add(1);
	}

	EnemyCharacter->UpdateFOVDisplay(TracePoints, Triangles, VertexColors);
}

EHSAIState AHSAIController::UpdateState(float DeltaTime)
{
	switch (State)
	{
	case EHSAIState::Idle:
		return ProcessStateIdle(DeltaTime);
	case EHSAIState::LookAround:
		return ProcessStateLookAround(DeltaTime);
	case EHSAIState::DetectTarget:
		return ProcessStateDetectTarget(DeltaTime);
	case EHSAIState::DetectNoise:
		return ProcessStateDetectNoise(DeltaTime);
	case EHSAIState::BackToHome:
		return ProcessStateBackToHome(DeltaTime);
	case EHSAIState::FindMisplacedItem:
		return ProcessStateFindMisplacedItem(DeltaTime);
	case EHSAIState::TakeItem:
		return ProcessStateTakeItem(DeltaTime);
	case EHSAIState::ReturnItem:
		return ProcessStateReturnItem(DeltaTime);
	}

	// If we don't have a handler for the current state - switch back to Idle
	return EHSAIState::Idle;
}

void AHSAIController::BeforeStateTransition(EHSAIState NewState)
{
	// Not used right now but can be useful
}

void AHSAIController::AfterStateTransition(EHSAIState OldState)
{
	switch (State)
	{
	case EHSAIState::Idle:
		ForgetSignal();
		StopMovement();

		LookAroundTimeout = FMath::FRandRange(LookAroundTimeoutMin, LookAroundTimeoutMax);

		HS_LOG("Begin Idle; Look Around is expected after: %f seconds", LookAroundTimeout);

		break;
	case EHSAIState::LookAround:
		LookAroundDirection = FMath::RandBool() ? 1.f : -1.f;
		break;
	case EHSAIState::DetectTarget:
		EnemyCharacter->SetMovementType(EHSMovementType::Chase);
		break;
	case EHSAIState::DetectNoise:
	case EHSAIState::TakeItem:
	case EHSAIState::ReturnItem:
		EnemyCharacter->SetMovementType(EHSMovementType::Run);
		break;
	case EHSAIState::BackToHome:
		EnemyCharacter->SetMovementType(EHSMovementType::Walk);
		break;
	case EHSAIState::GameOver:
		UHSEventsSubsystem* EventSubsystem = GetWorld()->GetSubsystem<UHSEventsSubsystem>();
		EventSubsystem->GameOver();

		break;
	}
}

/* (BEGIN) State Machine states */

EHSAIState AHSAIController::ProcessStateIdle(float DeltaTime)
{
	EHSAIState NewState;
	if (IsSignalDetected(NewState))
	{
		return NewState;
	}

	if (LastSignal.Type == EHSSignalType::Noise)
	{
		return EHSAIState::FindMisplacedItem;
	}

	const FVector Location = EnemyCharacter->GetActorLocation();
	const FVector HomeLocation = EnemyCharacter->GetHomeLocation();

	const float DistanceToHome = FVector::Dist2D(Location, HomeLocation);

	// If we're far from home - go back
	if (DistanceToHome > AcceptanceRadius)
	{
		return EHSAIState::BackToHome;
	}

	// TODO: Move this to Before/After state handlers
	// Restore initial rotation
	const FRotator HomeRotation = EnemyCharacter->GetHomeRotation();
	SetControlRotation(HomeRotation);

	// If it's time to look around...
	if (CurrentStateDuration >= LookAroundTimeout)
	{
		return EHSAIState::LookAround;
	}

	return State;
}

EHSAIState AHSAIController::ProcessStateLookAround(float DeltaTime)
{
	EHSAIState NewState;
	if (IsSignalDetected(NewState))
	{
		return NewState;
	}

	if (CurrentStateDuration > LookAroundDuration)
	{
		return EHSAIState::Idle;
	}

	FRotator ViewRotation = GetControlRotation();
	
	// Calculate the 360-degrees rotation during the time
	const float YawStep = LookAroundDirection * (360.f / LookAroundDuration) * DeltaTime;
	FQuat RotationDelta = FRotator(0.f, YawStep, 0.f).Quaternion();

	FQuat ResultingRotation = ViewRotation.Quaternion() * RotationDelta;
	ResultingRotation.Normalize();

	SetControlRotation(FRotator(ResultingRotation));

	return State;
}

EHSAIState AHSAIController::ProcessStateDetectTarget(float DeltaTime)
{
	const FVector Location = EnemyCharacter->GetActorLocation();
	const FVector Destination = DetectedSignal.Location;

	const float DistanceToTarget = FVector::Dist2D(Location, Destination);

	// Check if target is reached
	if (DistanceToTarget < AcceptanceRadius)
	{
		if (bCanSeeTarget)
		{
			// Reached the target location & can see it == caught!
			return EHSAIState::GameOver;
		}
		
		// Target location is reached but we can't see the target
		ForgetSignal();

		return EHSAIState::LookAround;
	}

	// Continue to move to the target
	if (!ProcessMovement(Destination))
	{
		// Just in case - if can't perform movement, then swithc to LookAround state
		return EHSAIState::LookAround;
	}

	return State;
}

EHSAIState AHSAIController::ProcessStateDetectNoise(float DeltaTime)
{
	EHSAIState NewState;
	if (IsSignalDetected(NewState))
	{
		return NewState;
	}

	const FVector Location = EnemyCharacter->GetActorLocation();
	const FVector Destination = DetectedSignal.Location;

	const float DistanceToTarget = FVector::Dist2D(Location, Destination);

	// Check if target is reached
	if (DistanceToTarget < AcceptanceRadius)
	{
		// Forget the current Noise signal - we already at place
		ForgetSignal();
		return EHSAIState::LookAround;
	}

	// Continue to move to the target
	if (!ProcessMovement(Destination))
	{
		// Just in case - if can't perform movement, then swithc to LookAround state
		return EHSAIState::LookAround;
	}

	return State;
}

EHSAIState AHSAIController::ProcessStateBackToHome(float DeltaTime)
{
	EHSAIState NewState;
	if (IsSignalDetected(NewState))
	{
		return NewState;
	}

	const FVector Location = EnemyCharacter->GetActorLocation();
	const FVector HomeLocation = EnemyCharacter->GetHomeLocation();

	const float DistanceToHome = FVector::Dist2D(Location, HomeLocation);

	// If we already at home
	if (DistanceToHome < AcceptanceRadius)
	{
		return EHSAIState::Idle;
	}

	// Continue to walk home
	if (!ProcessMovement(HomeLocation))
	{
		return EHSAIState::LookAround;
	}

	return State;
}

EHSAIState AHSAIController::ProcessStateFindMisplacedItem(float DeltaTime)
{
	ForgetSignal();

	const FVector Location = EnemyCharacter->GetActorLocation();

	const FCollisionShape Sphere = FCollisionShape::MakeSphere(FOVDistance);

	static const FName ReachOverlapName(TEXT("AIFindItems"));
	FCollisionQueryParams Params(ReachOverlapName);

	// Don't use the FOVTrace for finding the misplaced items
	// it's enough to have just a single sphere overlap
	TArray<FOverlapResult> Overlaps;
	GetWorld()->OverlapMultiByChannel(Overlaps, Location, FQuat::Identity, ECC_Visibility, Sphere, Params);

	float MinDistance = FOVDistance;
	AHSItemBase* MisplacedItem = nullptr;

	for (const FOverlapResult& Overlap : Overlaps)
	{
		AHSItemBase* Item = Cast<AHSItemBase>(Overlap.GetActor());
		if (!Item || Item->IsItemTaken())
		{
			continue;
		}

		const FVector ItemLocation = Item->GetActorLocation();
		const float DistanceToItemHome = FVector::Dist2D(ItemLocation, Item->GetHomeLocation());
		if (DistanceToItemHome < ItemAcceptanceDistance)
		{
			// The item is at it's home location
			continue;
		}

		const float DistanceToAI = FVector::Dist2D(ItemLocation, Location);
		if (DistanceToAI < MinDistance)
		{
			MinDistance = DistanceToAI;
			MisplacedItem = Item;
		}
	}

	if (MisplacedItem)
	{
		FHSSignal NewSignal(MisplacedItem);
		DetectSignal(NewSignal);
		return EHSAIState::TakeItem;
	}

	// Can't find a misplaced item nearby
	RememberLastSignal();
	return EHSAIState::Idle;
}

EHSAIState AHSAIController::ProcessStateTakeItem(float DeltaTime)
{
	// Test for Noise or Target signals
	EHSAIState NewState;
	if (IsSignalDetected(NewState))
	{
		return NewState;
	}

	if (DetectedSignal.Type != EHSSignalType::MisplacedItem || !DetectedSignal.TargetItem)
	{
		ForgetSignal();
		RememberLastSignal();
		return EHSAIState::LookAround;
	}

	const FVector Location = EnemyCharacter->GetActorLocation();
	const FVector Destination = DetectedSignal.TargetItem->GetActorLocation();

	// If we already at location - take item
	const float DistanceToTarget = FVector::Dist2D(Location, Destination);
	if (DistanceToTarget < AcceptanceRadius)
	{
		CarriedItem = DetectedSignal.TargetItem;
		CarriedItem->SetItemTaken(true);

		// Bring the item to it's location
		FHSSignal NewSignal(DetectedSignal.TargetItem);
		DetectSignal(NewSignal);
		return EHSAIState::ReturnItem;
	}

	if (!ProcessMovement(Destination))
	{
		return EHSAIState::LookAround;
	}

	return State;
}

EHSAIState AHSAIController::ProcessStateReturnItem(float DeltaTime)
{
	// Test for Noise or Target signals
	EHSAIState NewState;
	if (IsSignalDetected(NewState))
	{
		DropItem();
		return NewState;
	}

	UpdateItemLocation();

	const FVector Location = EnemyCharacter->GetActorLocation();
	const FVector Destination = DetectedSignal.TargetItem->GetHomeLocation();

	// If we already at location - drop item
	const float DistanceToTarget = FVector::Dist2D(Location, Destination);
	if (DistanceToTarget < ItemAcceptanceDistance)
	{
		CarriedItem->SetActorLocation(Destination);
		DropItem();

		ForgetSignal();
		RememberLastSignal();

		return EHSAIState::Idle;
	}

	if (!ProcessMovement(Destination))
	{
		return EHSAIState::LookAround;
	}

	return State;
}

/* (END) State Machine states */

bool AHSAIController::IsSignalDetected(EHSAIState& OutState)
{
	if (DetectedSignal == LastSignal)
	{
		// No new signals
		return false;
	}

	switch (DetectedSignal.Type)
	{
	case EHSSignalType::Noise:
		OutState = EHSAIState::DetectNoise;
		RememberLastSignal();
		return true;
	case EHSSignalType::Target:
		OutState = EHSAIState::DetectTarget;
		RememberLastSignal();
		return true;
	}

	return false;
}

void AHSAIController::RememberLastSignal()
{
	LastSignal = DetectedSignal;
}

void AHSAIController::DetectSignal(const FHSSignal& InSignal)
{
	bool bCanDetectSignal = false;
	
	const float DistanceToSignal = FVector::Dist2D(EnemyCharacter->GetActorLocation(), InSignal.Location);

	switch (InSignal.Type)
	{
	case EHSSignalType::Target:
		// Target signal detected - always handle it
		bCanDetectSignal = true;
		break;
	case EHSSignalType::Noise:
		if (DetectedSignal.Type == EHSSignalType::Target)
		{
			// Skip Noise signals if we have Target signal
			bCanDetectSignal = false;
			break;
		}

		// Check if the signal is not too far
		bCanDetectSignal = DistanceToSignal <= HearDistance;

		break;
	case EHSSignalType::MisplacedItem:
		if (DetectedSignal.Type == EHSSignalType::Target || DetectedSignal.Type == EHSSignalType::Noise)
		{
			// Skip MisplacedItem signals if we have more important one
			bCanDetectSignal = false;
			break;
		}

		// Check if the signal is not too far
		bCanDetectSignal = DistanceToSignal <= HearDistance;

		break;
	}

	if (!bCanDetectSignal)
	{
		// Skip the signal - too far
		return;
	}

	DetectedSignal = InSignal;
}

void AHSAIController::ForgetSignal()
{
	DetectedSignal = FHSSignal::None;
}

bool AHSAIController::ProcessMovement(const FVector& Destination)
{
	FVector ClosestPoint;
	// Try to find the point on existing nav mesh
	if (!EnemyCharacter->FindNextNavigationPoint(Destination, ClosestPoint))
	{
		return false;
	}

	EPathFollowingRequestResult::Type Result = MoveToLocation(ClosestPoint, -1, false, true, true, true);

	return Result != EPathFollowingRequestResult::Type::Failed;
}

void AHSAIController::OnNoiseDetected(FVector Origin, AActor* Source)
{
	FHSSignal NewSignal(Origin);
	DetectSignal(NewSignal);
}

void AHSAIController::UpdateItemLocation()
{
	if (!CarriedItem)
	{
		return;
	}

	const FVector Location = EnemyCharacter->GetActorLocation();
	const FRotator Rotation = EnemyCharacter->GetActorRotation();

	const FVector ItemOffset = Rotation.RotateVector(FVector(70.f, 0.f, 40.f));
	const FVector NewLocation = Location + ItemOffset;

	FHitResult HitResult;
	CarriedItem->SetActorLocation(NewLocation, true, &HitResult);
	if (HitResult.bBlockingHit)
	{
		// Quick hack to move the taken object when blocked by something
		CarriedItem->SetActorLocation(Location);
		CarriedItem->SetActorLocation(NewLocation, true);
	}
}

void AHSAIController::DropItem()
{
	CarriedItem->SetItemTaken(false);
	CarriedItem = nullptr;
}
