// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"

#include "HS/Core/HSTypes.h"
#include "HSAIController.generated.h"

class APawn;
class AHSCharacter;

/**
 * Enemy AI controller
 */
UCLASS()
class HS_API AHSAIController : public AAIController
{
	GENERATED_BODY()

public: // AAIController

	AHSAIController();

	virtual void Tick(float DeltaTime) override;

protected: // AAIController

	virtual void BeginPlay() override;

	virtual void OnPossess(APawn* InPawn) override;
	virtual void OnUnPossess() override;

public: // Methods

	// Field of view tracing
	// Updates both Target signal (if a Target is found) and FOVDisplay visuals
	UFUNCTION(BlueprintCallable, Category = "Hide and Seek|AI")
	void FOVTrace(float Angle);

	// Register a new signal
	// AI does not have memory, so the previous signal will be replaced if the new signal has the same or higher priority
	UFUNCTION(BlueprintCallable, Category = "Hide and Seek|AI")
	void DetectSignal(const FHSSignal& InSignal);

	// Forget current known signal
	UFUNCTION(BlueprintCallable, Category = "Hide and Seek|AI")
	void ForgetSignal();

	// Drop current carried item
	UFUNCTION(BlueprintCallable, Category = "Hide and Seek|AI")
	void DropItem();

	// Return true if a signal was detected
	// OutState will contain the appropriate state for the signal
	bool IsSignalDetected(EHSAIState& OutState);

	// Remember the current detected signal, if any, for later usage
	void RememberLastSignal();

	// Request nav movement
	// Returns false if movement request can't be processed
	bool ProcessMovement(const FVector& Destination);

protected: // Internal methods

	// Triggered after all objects spawn is completed
	UFUNCTION()
	void OnGameStarted();

	// Update the AI state based on current one and other parameters
	// Should return the new state
	EHSAIState UpdateState(float DeltaTime);

	// Executed at the end of previous state
	void BeforeStateTransition(EHSAIState NewState);

	// Executed right after switched to the new state
	void AfterStateTransition(EHSAIState OldState);

	// Handler for detected noise events
	UFUNCTION()
	void OnNoiseDetected(FVector Origin, AActor* Source);

	// Update the carried item location
	void UpdateItemLocation();

	// Process State Machine states
	// Should return the new state (can be the same - if no transition is required)

	EHSAIState ProcessStateIdle(float DeltaTime);
	EHSAIState ProcessStateLookAround(float DeltaTime);
	EHSAIState ProcessStateDetectTarget(float DeltaTime);
	EHSAIState ProcessStateDetectNoise(float DeltaTime);
	EHSAIState ProcessStateBackToHome(float DeltaTime);
	EHSAIState ProcessStateFindMisplacedItem(float DeltaTime);
	EHSAIState ProcessStateTakeItem(float DeltaTime);
	EHSAIState ProcessStateReturnItem(float DeltaTime);

protected: // Settings

	// Minimal size of a target which should be detected
	// Used to calculate the required rays count for tracing
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hide and Seek|FOV", meta = (ClampMin = 1))
	float MinimalTargetSize;

	// Offset for trace starting location
	// Default trace start is at the actor's location
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hide and Seek|FOV")
	FVector FOVOffset;

	// Offset for FOV visuals
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hide and Seek|FOV")
	FVector FOVDisplayOffset;

	// Max field of view distance
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hide and Seek|FOV", meta = (ClampMin = 1))
	float FOVDistance;

	// Field of view angle
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hide and Seek|FOV", meta = (ClampMin = 1, ClampMax = 360))
	float FOVAngle;

	// Max distance at which the AI can hear the noise signal
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hide and Seek|Items")
	float HearDistance;

	// Max distance at which the item is considered to be at "home" location
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hide and Seek|Items")
	float ItemAcceptanceDistance;

	// Minimal timeout between looking around
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hide and Seek|AI", meta = (ClampMin = 0))
	float LookAroundTimeoutMin;

	// Maximum timeout between looking around
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hide and Seek|AI", meta = (ClampMin = 0))
	float LookAroundTimeoutMax ;

	// Duration of looking around
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hide and Seek|AI", meta = (ClampMin = 0))
	float LookAroundDuration;

	// How close AI should move to the destination location
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hide and Seek|Navigation", meta = (ClampMin = -1))
	float AcceptanceRadius;

protected: // Variables

	UPROPERTY(BlueprintReadOnly, Category = "Hide and Seek|FOV")
	float MaximumStepAngle;

	UPROPERTY(BlueprintReadOnly, Category = "Hide and Seek|AI")
	EHSAIState State;

	UPROPERTY(BlueprintReadOnly, Category = "Hide and Seek|AI")
	float CurrentStateDuration = 0.f;

	UPROPERTY(BlueprintReadOnly, Category = "Hide and Seek|AI")
	float LookAroundTimeout = 0.f;

	UPROPERTY(BlueprintReadOnly, Category = "Hide and Seek|AI")
	float LookAroundDirection = 1.f;

	UPROPERTY(BlueprintReadOnly, Category = "Hide and Seek|AI")
	FHSSignal DetectedSignal;

	UPROPERTY(BlueprintReadOnly, Category = "Hide and Seek|AI")
	FHSSignal LastSignal;

	UPROPERTY(BlueprintReadOnly, Category = "Hide and Seek|AI")
	bool bCanSeeTarget;

	UPROPERTY(BlueprintReadOnly, Category = "Hide and Seek|Items")
	AHSItemBase* CarriedItem = nullptr;

protected: // Cache

	UPROPERTY(BlueprintReadOnly, Category = "Hide and Seek")
	AHSCharacter* EnemyCharacter = nullptr;

};
